import java.util.LinkedList;
/** Realiseerida abstraktne andmetüüp "reaalarvude magasin" (LIFO) ahela (linkedlist) abil */

public class DoubleStack {

	/** Peameetod
	 * @param doubleList magasin */
	public static void main(String[] argum) {
		
		DoubleStack doubleList = new DoubleStack();

		System.out.println(doubleList);
		doubleList.push(5);
		doubleList.op("x");
		System.out.println(doubleList);
		doubleList.push(8);
		System.out.println(doubleList);
		doubleList.push(3);
		System.out.println(doubleList);
		doubleList.push(4);
		System.out.println(doubleList);
		doubleList.op("+");
		System.out.println(doubleList);
		doubleList.op("-");
		System.out.println(doubleList);
		doubleList.op("/");
		System.out.println(doubleList);
		doubleList.op("*");
		System.out.println(doubleList);
		System.out.println(DoubleStack.interpret("2 15 -"));
	}

	/** Uus list
	 * https://docs.oracle.com/javase/7/docs/api/java/util/LinkedList.html
	 * @param list list */
	private LinkedList<Double> list;

	
	/** Uue magasini konstruktor
	 * @param list list */
	DoubleStack() {

		list = new LinkedList<Double>();
	}
	

	/** Koopia loomine
	 * @param list list
	 * @param kloon magasin
	 * @return tagastab listi koopia
	 * @see http://www.tutorialspoint.com/java/util/linkedlist_clone.htm */
	@SuppressWarnings("unchecked")
	@Override
	public Object clone() throws CloneNotSupportedException {

		DoubleStack kloon = new DoubleStack();
		kloon.list = (LinkedList<Double>) list.clone();

		return kloon;
	}
	

	/** Kontrollib, kas magasin on tühi
	 * @param list list
	 * @return tagastab true kui magasin tühi
	 * @see https://docs.oracle.com/javase/7/docs/api/java/lang/String.html#isEmpty() */
	public boolean stEmpty() {
		
		return list.isEmpty();
	}
	

	/** Magasini elemendi lisamine
	 * @param a element, mida lisatakse magasini
	 * @param list list
	 * @see https://docs.oracle.com/javase/7/docs/api/java/util/LinkedList.html#push(E) */
	public void push(double a) {

		list.push(a);
	}
	

	/** Magasinist elemendi võtmine
	 * @param list list
	 * @return tagastab magasini esimese elemendi
	 * @see https://docs.oracle.com/javase/7/docs/api/java/util/LinkedList.html#pop() */
	public double pop() {
		
		if (this.stEmpty()) {

			throw new RuntimeException("Avaldist pole võimalik teostada. Palun sisesta avaldis.");
		}
		
		else{

		return list.pop();
		
		}
	}
	
	
	/** Aritmeetikatehe s ( + - * / ) magasini kahe pealmise elemendi vahel 
	 * (tulemus pannakse uueks tipuks): void op (String s)
	 * @param s sisestatud tehe ( + - * / )
	 * @param list list
	 * @param op2 esimene element magasini tipust
	 * @param op1 teine element mahasini tipust
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/adt.html
	 * @see http://www.programcreek.com/2012/12/leetcode-evaluate-reverse-polish-notation/ */
	public void op(String s) {

		if (this.stEmpty()) {

			throw new RuntimeException("Avaldist pole võimalik teostada. Palun sisesta aritmeetiline avaldis");
		}

		else {

			if (s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/")) {

				double op2 = pop();
				double op1 = pop();

				if (s.equals("+")) {
					push(op1 + op2);
				}

				if (s.equals("-")) {
					push(op1 - op2);
				}

				if (s.equals("*")) {
					push(op1 * op2);
				}

				if (s.equals("/")) {
					push(op1 / op2);
				}
			} else {

				throw new RuntimeException("Tehet " +s+ " pole võimalik teha. Kasuta +, -, * ja / tehteid. ");

			}

		}
	}

	/** Tipu lugemine eemaldamiseta
	 * @param list list
	 * @return tagastab esimese elemendi magasinis
	 * @see https://docs.oracle.com/javase/7/docs/api/java/util/LinkedList.html#peekFirst() */
	public double tos() {
		
		if (this.stEmpty()) {

			throw new RuntimeException("Hetkel pole sisestatud avaldise jaoks ühtegi elementi. Palun sisesta avaldis.");
		}
		
		else {
			return list.peekFirst();
		}
	}

	/** Kahe magasini võrdsuse kindlakstegemine
	 * @param o võrreldav magasin
	 * @param list list
	 * @return tagastab true, kui võdrsed. False kui pole
	 * @see http://www.java2s.com/Tutorial/Java/0060__Operators/TheinstanceofKeyword.htm */
	@Override
	public boolean equals(Object o) {

		if (o instanceof DoubleStack) {
			return list.equals(((DoubleStack) o).list);
		} 
		
		else
			return false;
	}
	

	/** Teisendus sõneks (tipp lõpus)
	 * @param sb string kuhu pannakse magasinist tulev element
	 * @param list list
	 * @param i magasini viimane element
	 * @return tagastab sõne
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/adt.html */
	@Override
	public String toString() {

		if (stEmpty()) {
			
			return "Avaldises pole ühtegi elementi.";
		}

		else {
			
			StringBuilder sb = new StringBuilder();

			for (int i = list.size() - 1; i > 0; i--) {

				sb.append(list.get(i)).append(" ");

			}

			return sb.toString();
		}
	}
	

	/** Aritmeetilise avaldise pööratud poola kuju (sulgudeta postfikskuju, Reverse Polish Notation) 
	 * pol interpreteerimiseks (väljaarvutamiseks) eelpool defineeritud reaalarvude magasini abil. 
	 * Avaldis on antud stringina, mis võib sisaldada reaalarve (s.h. negatiivseid ja mitmekohalisi) 
	 * ning tehtemärke + - * / , mis on eraldatud tühikutega (whitespace). Tulemuseks peab olema avaldise 
	 * väärtus reaalarvuna või erindi (RuntimeException) tekitamine, kui avaldis ei ole korrektne. 
	 * Korrektne ei ole, kui avaldises esineb lubamatuid sümboleid, kui avaldis jätab magasini üleliigseid 
	 * elemente või kasutab magasinist liiga palju elemente. Näit. DoubleStack.interpret ("2. 15. -") peaks 
	 * tagastama väärtuse -13.
	 * @param pol aritmeetiline avaldis
	 * @param stack magasin
	 * @param elements Stringi massiiv, aritmeetilisest avaldisest eraldatakse elemendid Stringi kaupa
	 * @param element üksik element Stringi kujul massiivis
	 * @param a Double tüüpi element
	 * @param b tehtemärk
	 * @return tagastab reaalarvu, mis saadakse aritmeetilise avaldise tegemisest
	 * @see http://www.programcreek.com/2012/12/leetcode-evaluate-reverse-polish-notation/ */
	public static double interpret(String pol) {

		if (pol == null || pol.length() == 0) {

			throw new RuntimeException("Palun sisesta avaldis.");
		}

		else {

			DoubleStack stack = new DoubleStack(); //teeb uue magasini

			String[] elements = pol.trim().split("\\s+"); //eraldab tühikute kohapealt elemendi

			int a = 0;
			int b = 0;

			for (String element : elements) {

				try {
					stack.push(Double.valueOf(element)); //peab olema Double tüüpi element, siis lisatakse magasini
					a++;

				} catch (NumberFormatException e) {

					if (stack.list.size() < 2)

						throw new RuntimeException("Avaldise tegemiseks on liiga vähe elemente. Viga on avaldises " + pol);

					else if (!element.equals("+") && !element.equals("-") && !element.equals("*")
							&& !element.equals("/"))

						throw new RuntimeException(pol + " avaldisse sisestatud '" + element + "' tehe pole lubatud. Kasuta +, -, * ja / tehteid.");

					else if (stack.stEmpty())

						throw new RuntimeException("Palun lisa avaldis " + pol);
					
					

					stack.op(element);
					b++;
				}
			}

			if (a - 1 != b)
				throw new RuntimeException("Avaldist " + pol + " pole võimalik teostada.");
			return stack.pop();

		}
	}

}
